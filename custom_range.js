define([
	'require',
],
function (require){
	var CustomRange = {
		init : function(range){
			var grades = range.querySelector('.custom-range-grades');
			var input = range.querySelector('input[type=range]');
			grades.innerHTML = '';
			var total = (input.max-input.min)/input.step;
			for(var i=0; i<total; i++){
				var grade = document.createElement('div');
					grade.style.width = 99/total + '%';
				grades.appendChild(grade);
			}
			CustomRange.update({target : input});
			input.addEventListener('input', CustomRange.update, false);
			input.addEventListener('change', CustomRange.update, false);
		},
		update : function(e){
			var input = e.target;
			var range = e.target.parentNode;
			var grades = range.querySelectorAll('.custom-range-grades>div');

			for(var i=0; i<(input.value-input.min)/input.step; i++) grades[i].className = 'active';
			for(var i=(input.value-input.min)/input.step; i<(input.max-input.min)/input.step; i++) grades[i].className = '';
		}
	};

	ranges = document.querySelectorAll('.custom-range');
	for(var i=0; i<ranges.length; i++) CustomRange.init(ranges[i]);

	return CustomRange;
});