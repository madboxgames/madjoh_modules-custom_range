# custom_range #

v1.0.3

## Getting Started ##

To get this to work, put this in your html :

```html
<label class="custom-range">
	<div class="custom-range-grades">
	</div>
	<input type="range" min="0" max="4" step="1" value="2"/>
</label>
```

More details coming soon.